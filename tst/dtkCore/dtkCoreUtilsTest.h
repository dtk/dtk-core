// dtkCoreUtilsTest.h ---
//

#pragma once

#include <QtCore>

class dtkCoreUtilsTestCase : public QObject
{
    Q_OBJECT

public:
     dtkCoreUtilsTestCase(void);
    ~dtkCoreUtilsTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testForEach(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkCoreUtilsTestCasePrivate *d;
};

//
// dtkCoreUtilsTest.h ends here
