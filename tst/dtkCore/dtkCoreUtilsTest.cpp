// dtkCoreUtilsTest.cpp ---
//

#include "dtkCoreUtilsTest.h"

#include <dtkCoreTest>

#include <dtkCore/dtkCoreUtils>
#include <dtkCore/dtkCoreMetaType>

// ///////////////////////////////////////////////////////////////////
// dtkCoreUtilsTestCasePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkCoreUtilsTestCasePrivate
{
public:
};

// ///////////////////////////////////////////////////////////////////

dtkCoreUtilsTestCase::dtkCoreUtilsTestCase(void) : d(new dtkCoreUtilsTestCasePrivate)
{
}

dtkCoreUtilsTestCase::~dtkCoreUtilsTestCase(void)
{
    delete d;
}

void dtkCoreUtilsTestCase::initTestCase(void)
{

}

void dtkCoreUtilsTestCase::init(void)
{
}

void dtkCoreUtilsTestCase::testForEach(void)
{
    // Init the tuple
    auto t = std::make_tuple(-5, 3.14159, QString("Hello"), QVariant::fromValue(QList<double>({1.1,2.2,3.3})));

    // Type and size of the tuple
    using TupleType = std::decay_t<decltype(t)>;
    constexpr std::size_t N = std::tuple_size_v<TupleType>;

    // We want to parse the tuple and fill a dynamic QList with the MetaType of each type of the tuple

    // init the list
    QList<QMetaType> metatypes(N);


    // Loop over the tuple using for_each function of dtkCoreUtils
    std::size_t id = 0;
    ::dtk::for_each(t, [&](auto&& elt) {
        using Type = std::decay_t<decltype(elt)>;
        metatypes[id] = QMetaType::fromType<Type>();
        ++id;
    }
    );

    // Check that the types match
    using Type0 = std::decay_t<decltype(std::get<0>(t))>;
    auto mt = QMetaType::fromType<Type0>();
    QCOMPARE(metatypes[0], mt);

    using Type1 = std::decay_t<decltype(std::get<1>(t))>;
    mt = QMetaType::fromType<Type1>();
    QCOMPARE(metatypes[1], mt);

    using Type2 = std::decay_t<decltype(std::get<2>(t))>;
    mt = QMetaType::fromType<Type2>();
    QCOMPARE(metatypes[2], mt);

    using Type3 = std::decay_t<decltype(std::get<3>(t))>;
    mt = QMetaType::fromType<Type3>();
    QCOMPARE(metatypes[3], mt);
}

void dtkCoreUtilsTestCase::cleanupTestCase(void)
{

}

void dtkCoreUtilsTestCase::cleanup(void)
{
}

DTKCORETEST_MAIN_NOGUI(dtkCoreUtilsTest, dtkCoreUtilsTestCase);

//#include "dtkCoreUtilsTest.moc"

//
// dtkCoreUtilsTest.cpp ends here
