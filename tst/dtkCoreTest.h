#pragma once

#include <QtTest>
#include <dtkLog>
#define DTKCORETEST_MAIN_NOGUI(TestMain, TestObject)    \
  int TestMain(int argc, char *argv[]) {                \
        QCoreApplication app(argc, argv);               \
        dtkLogger::instance().attachConsole();          \
        dtkLogger::instance().setLevel("Debug");        \
        TestObject tc;                                  \
        return QTest::qExec(&tc, argc, argv);           \
    }
