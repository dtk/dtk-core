### CMakeLists.txt ---
##

add_subdirectory(dtkCore)
add_subdirectory(dtkCoreRuntime)

######################################################################
### CMakeLists.txt ends here
