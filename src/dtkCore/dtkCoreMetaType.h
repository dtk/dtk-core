// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCoreExport>

#include "dtkCoreTypeTraits.h"

#include <QtCore>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class DTKCORE_EXPORT dtkCoreMetaTypeRegistry
{
    static dtkCoreMetaTypeRegistry *s_instance;

     dtkCoreMetaTypeRegistry(void) = default;
    ~dtkCoreMetaTypeRegistry(void) = default;

public:
    static dtkCoreMetaTypeRegistry *instance(void);

    void insert(int metatype_id, const QString& alias);
    QString alias(int metatype_id) const;

    QMap<int, QString> aliases;
};

namespace dtk {
    DTKCORE_EXPORT dtkCoreMetaTypeRegistry& metaTypeRegistry(void);
}

// ///////////////////////////////////////////////////////////////////
// dtkCoreMetaType functions declarations
// ///////////////////////////////////////////////////////////////////

namespace dtk {

    template <typename T> bool canConvert(const QList<int>&);

    template <typename T> QVariant variantFromValue(const T&);

#ifndef SWIG
    template <typename... Args> QVariantList variantListFromTuple(const std::tuple<Args...>&);
    template <typename... Args> QList<std::pair<QMetaType, std::uintptr_t>> metaTypeListFromTuple(const std::tuple<Args...>&);
#endif

    template <typename T> T *clone(const T *);
    template <typename T> bool copy(const T *, T *);

    template <typename T> void    assign(T& lhs, const T& rhs);
    template <typename T> void addAssign(T& lhs, const T& rhs);
    template <typename T> void subAssign(T& lhs, const T& rhs);
    template <typename T> void mulAssign(T& lhs, const T& rhs);
    template <typename T> void divAssign(T& lhs, const T& rhs);

    DTKCORE_EXPORT  QString description(const QVariant& v);
    DTKCORE_EXPORT QVariant cloneContent(const QVariant& v);
    DTKCORE_EXPORT QVariant createDefaultConstructedType(const QVariant& v);
    DTKCORE_EXPORT     bool destroyPointer(QVariant& v);
}

// /////////////////////////////////////////////////////////////////
// Stream operators redefinition
// /////////////////////////////////////////////////////////////////

template <typename T> QDataStream& operator << (QDataStream& s, const std::list<T>& l);
template <typename T> QDataStream& operator >> (QDataStream& s,       std::list<T>& l);

template <typename T> QDataStream& operator << (QDataStream& s, const std::vector<T>& v);
template <typename T> QDataStream& operator >> (QDataStream& s,       std::vector<T>& v);

template <typename T> QDataStream& operator << (QDataStream& s, QList<T> * l);
template <typename T> QDataStream& operator >> (QDataStream& s, QList<T>*& l);

template <typename T> QDataStream& operator << (QDataStream& s, std::list<T> *l);
template <typename T> QDataStream& operator >> (QDataStream& s, std::list<T>*&l);

template <typename T> QDataStream& operator << (QDataStream& s, std::vector<T> *v);
template <typename T> QDataStream& operator >> (QDataStream& s, std::vector<T>*&v);

template <typename T> QDataStream& operator << (QDataStream& s, const QList<T *>& v);
template <typename T> QDataStream& operator >> (QDataStream& s,       QList<T *>& v);

template <typename T> QDataStream& operator << (QDataStream& s, const std::list<T *>& l);
template <typename T> QDataStream& operator >> (QDataStream& s,       std::list<T *>& l);

template <typename T> QDataStream& operator << (QDataStream& s, const std::vector<T *>& v);
template <typename T> QDataStream& operator >> (QDataStream& s,       std::vector<T *>& v);

// ///////////////////////////////////////////////////////////////////
// QMetaType for std::string
// ///////////////////////////////////////////////////////////////////

Q_DECLARE_METATYPE(std::string)

DTKCORE_EXPORT QDebug& operator << (QDebug&, const std::string&);

DTKCORE_EXPORT QDataStream& operator << (QDataStream&, const std::string&);
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&,       std::string&);

// /////////////////////////////////////////////////////////////////

#include "dtkCoreMetaType.tpp"

//
// dtkCoreMetaType.h ends here
