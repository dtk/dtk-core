// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkCoreMetaType.h"
#include "dtkCoreMetaContainerSequential.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////////////////
// dtkCoreMetaTypeRegistry
// /////////////////////////////////////////////////////////////////////////////

dtkCoreMetaTypeRegistry *dtkCoreMetaTypeRegistry::instance(void)
{
    if (!s_instance) {
        s_instance = new dtkCoreMetaTypeRegistry;
    }

    return s_instance;
}

void dtkCoreMetaTypeRegistry::insert(int metatype_id, const QString& alias)
{
    aliases.insert(metatype_id, alias);
}

QString dtkCoreMetaTypeRegistry::alias(int metatype_id) const
{
    return aliases.value(metatype_id);
}

dtkCoreMetaTypeRegistry *dtkCoreMetaTypeRegistry::s_instance = nullptr;

namespace dtk {
    dtkCoreMetaTypeRegistry& metaTypeRegistry(void)
    {
        return *(dtkCoreMetaTypeRegistry::instance());
    }
}

// ///////////////////////////////////////////////////////////////////
// Debug and DataStream for std::string
// ///////////////////////////////////////////////////////////////////

QDebug& operator << (QDebug &dbg, const std::string& s)
{
    const bool old_setting = dbg.autoInsertSpaces();
    dbg.nospace() << s.c_str();
    dbg.setAutoInsertSpaces(old_setting);
    return dbg.maybeSpace();
}

QDataStream &operator<<(QDataStream& out, const std::string& s)
{
    QString tmp = QString::fromStdString(s);
    out << tmp;
    return out;
}

QDataStream &operator>>(QDataStream& in, std::string& s)
{
    QString tmp;
    in >> tmp;
    s = tmp.toStdString();
    return in;
}

// ///////////////////////////////////////////////////////////////////
// dtkCoreMetaType function implementations
// ///////////////////////////////////////////////////////////////////

namespace dtk
{
    // Description of the content of a QVariant
    QString description(const QVariant& v)
    {
        QString str;
        QDebug dbg(&str);
        dbg.setAutoInsertSpaces(false);

        if (v.canConvert<dtkCoreMetaContainerSequential>()) {
            dtkCoreMetaContainerSequential mc = v.value<dtkCoreMetaContainerSequential>();
            dbg << mc;
        } else {
            const uint typeId = v.userType();
            bool userStream = false;
            bool canConvertToString = false;

            if (typeId >= QMetaType::User) {
                userStream = QMetaType(typeId).debugStream(dbg, v.constData());
                canConvertToString = v.canConvert<QString>();
            }

            if (!userStream) {
                if (canConvertToString) {
                    dbg << v.toString();

                } else if (typeId == QMetaType::QVariantList
                        || v.canConvert<QSequentialIterable>()) {
                    QSequentialIterable iterable = v.value<QSequentialIterable>();
                    for (const QVariant& it : iterable) {
                        dbg << dtk::description(it);
                        dbg << '\n' ;
                    }

                } else if (typeId == QMetaType::QVariantMap
                        || typeId == QMetaType::QVariantHash
                        || v.canConvert<QAssociativeIterable>()) {
                    QAssociativeIterable iterable = v.value<QAssociativeIterable>();
                    for (auto it = iterable.begin(); it != iterable.end(); ++it) {
                        dbg << dtk::description(it.key()) << ": " << dtk::description(it.value());
                        dbg << '\n' ;
                    }

                } else {
                    dbg << qSetRealNumberPrecision( 15 )  << v;
                    QString cleanStr = str.trimmed();
                    cleanStr.chop(1);
                    // remove "QVariant(typename, " from the string:
                    auto count = 11 + QString(v.typeName()).size();
                    str = cleanStr.remove(0, count);
                }
            }
        }

        return str;
    }

    // Clones the content of a QVariant and stores it into another QVariant
    QVariant cloneContent(const QVariant& v)
    {
        auto mt = v.metaType();
        QVariant res;

        if (!mt.isValid()) {
            return res;

        } else if (mt.id() < QMetaType::User
                || (!(mt.flags() & QMetaType::IsPointer))) {
            // Delegates copy to the QVariant's copy operator for built-in types and no pointer types
            res = v;

        } else if (mt.flags() & QMetaType::PointerToQObject) {
            auto mo = mt.metaObject();
            auto mt_source = QMetaType::fromName(mo->className());

            // Try to find the most specific derived class to perform the copy
            while (!mt_source.isValid()) {
                mo = mo->superClass();
                if (!mo) {
                    break;
                }
                mt_source = QMetaType::fromName(mo->className());
            }

            if (mt_source.isValid()) {
                const void *source = *static_cast<const void *const *>(v.data());
                auto target = mt_source.create(source);
                QString type_name(mt_source.name());
                auto mt_target = QMetaType::fromName(qPrintable(type_name + "*"));
                if (mt_target.isValid()) {
                    res = QVariant(mt_target, &target);
                } else {
                    res = QVariant(mt, &target);
                }

            } else {
                QString type_name(mt.metaObject()->className());
                dtkTrace() << Q_FUNC_INFO << "Type" << type_name << "has not be registered to QMetaType System or it is not copyable. If they exist, parent classes are neither registered.";
            }

        } else if (mt.flags() & QMetaType::IsPointer) {

            QString type_name = mt.name();
            type_name.chop(1);

            auto mt_source = QMetaType::fromName(qPrintable(type_name));
            if (mt_source.isValid()) {
                const void *source = *static_cast<const void *const *>(v.data());
                auto target = mt_source.create(source);
                auto mt_target = QMetaType::fromName(qPrintable(type_name + "*"));
                if (mt_target.isValid()) {
                    res = QVariant(mt_target, &target);
                } else {
                    res = QVariant(mt, &target);
                }

            } else {
                dtkTrace() << Q_FUNC_INFO << "Type" << type_name << "has not be registered to QMetaType System or is not copyable.";
            }

        } else {
            // Should never go through this
            dtkTrace() << Q_FUNC_INFO << "Type" << mt.name() << "has not be registered to QMetaType System or is not copyable.";
        }

        return res;
    }

    // Creates a default constructed type into a QVariant. The result is of
    // the same type than the one contained in the input QVariant.
    QVariant createDefaultConstructedType(const QVariant& v)
    {
        auto mt = v.metaType();
        QVariant res;

        if (!mt.isValid()) {
            return res;

        } else if (mt.id() < QMetaType::User
                || (!(mt.flags() & QMetaType::IsPointer))) {
            res = QVariant(mt);

        } else if (mt.flags() & QMetaType::PointerToQObject) {
            auto mo = mt.metaObject();
            auto mt_source = QMetaType::fromName(mo->className());

            // Try to find the most specific derived class to perform the copy
            while (!mt_source.isValid()) {
                mo = mo->superClass();
                if (!mo) {
                    break;
                }
                mt_source = QMetaType::fromName(mo->className());
            }

            if (mt_source.isValid()) {
                auto target = mt_source.create();
                QString type_name(mt_source.name());
                auto mt_target = QMetaType::fromName(qPrintable(type_name + "*"));
                if (mt_target.isValid()) {
                    res = QVariant(mt_target, &target);
                } else {
                    res = QVariant(mt, &target);
                }

            } else {
                QString type_name(mt.metaObject()->className());
                dtkTrace() << Q_FUNC_INFO << "Type" << type_name << "has not be registered to QMetaType System or it is not default constructible. If they exist, parent classes are neither registered.";
            }

        } else if (mt.flags() & QMetaType::IsPointer) {

            QString type_name = mt.name();
            type_name.chop(1);

            auto mt_source = QMetaType::fromName(qPrintable(type_name));
            if (mt_source.isValid()) {
                auto target = mt_source.create();
                auto mt_target = QMetaType::fromName(qPrintable(type_name + "*"));
                if (mt_target.isValid()) {
                    res = QVariant(mt_target, &target);
                } else {
                    res = QVariant(mt, &target);
                }

            } else {
                dtkTrace() << Q_FUNC_INFO << "Type" << type_name << "has not be registered to QMetaType System or is not default constructible.";
            }

        } else {
            // Should never go through this
            dtkTrace() << Q_FUNC_INFO << "Type" << mt.name() << "has not be registered to QMetaType System or is not default constructible.";
        }

        return res;
    }


    // Destroys the pointer contained in the input QVariant
    bool destroyPointer(QVariant& v)
    {
        auto mt = v.metaType();
        bool ok = false;

        if (!mt.isValid()) {
            return ok;

        } else if (mt.flags() & QMetaType::IsPointer) {
            void *ptr = *static_cast<void **>(v.data());
            if (!ptr) {
                return ok;
            }

            QString type_name = mt.name();
            type_name.chop(1);
            QMetaType::fromName(qPrintable(type_name)).destroy(ptr);
            v.clear();
            ok = true;

        } else {
            dtkTrace() << Q_FUNC_INFO << "Type" << mt.name() << "is not of pointer type.";
        }

        return ok;
    }

}

//
// dtkCoreMetaType.cpp ends here
