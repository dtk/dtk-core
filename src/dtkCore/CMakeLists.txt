### CMakeLists.txt ---
##

project(dtkCore
VERSION
  ${dtkCore_VERSION}
LANGUAGES
  CXX)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkCore
  dtkCoreApplication_p
  dtkCoreApplication_p.h
  dtkCoreApplication
  dtkCoreApplication.h
  dtkCoreAsKeyValueRange
  dtkCoreAsKeyValueRange.h
  dtkCoreIOCompressor
  dtkCoreIOCompressor.h
  dtkCoreLayerManager
  dtkCoreLayerManager.h
  dtkCoreMetaContainerSequential
  dtkCoreMetaContainerSequential.h
  dtkCoreMetaContainerSequential.tpp
  dtkCoreMetaContainerSequentialHandler
  dtkCoreMetaContainerSequentialHandler.h
  dtkCoreMetaContainerSequentialHandler.tpp
  dtkCoreMetaType
  dtkCoreMetaType.h
  dtkCoreMetaType.tpp
  dtkCoreObjectManager
  dtkCoreObjectManager.h
  dtkCoreParameters
  dtkCoreParameters.h
  dtkCoreParameter
  dtkCoreParameter.h
  dtkCoreParameter.tpp
  dtkCoreParameterInList
  dtkCoreParameterInList.h
  dtkCoreParameterInList.tpp
  dtkCoreParameterInListStringList
  dtkCoreParameterInListStringList.h
  dtkCoreParameterNumeric
  dtkCoreParameterNumeric.h
  dtkCoreParameterNumeric.tpp
  dtkCoreParameterPath
  dtkCoreParameterPath.h
  dtkCoreParameterRange
  dtkCoreParameterRange.h
  dtkCoreParameterRange.tpp
  dtkCoreParameterSimple
  dtkCoreParameterSimple.h
  dtkCoreParameterSimple.tpp
  dtkCoreParameterObject
  dtkCoreParameterObject.h
  dtkCoreParameterInListObject
  dtkCoreParameterInListObject.h
  dtkCoreParameterInListStringListObject
  dtkCoreParameterInListStringListObject.h
  dtkCoreParameterPathObject
  dtkCoreParameterPathObject.h
  dtkCoreParameterNumericObject
  dtkCoreParameterNumericObject.h
  dtkCoreParameterRangeObject
  dtkCoreParameterRangeObject.h
  dtkCoreParameterSimpleObject
  dtkCoreParameterSimpleObject.h
  dtkCoreParameterCollection
  dtkCoreParameterCollection.h
  dtkCorePlugin
  dtkCorePlugin.h
  dtkCorePluginBase
  dtkCorePluginBase.h
  dtkCorePluginFactory
  dtkCorePluginFactory.h
  dtkCorePluginFactory.tpp
  dtkCorePluginFactory_p.h
  dtkCorePluginFactoryRecorder
  dtkCorePluginFactoryRecorder.h
  dtkCorePluginManager
  dtkCorePluginManager.h
  dtkCorePluginManager.tpp
  dtkCorePluginManagerHandler
  dtkCorePluginManagerHandler.h
  dtkCorePluginManagerRecorder
  dtkCorePluginManagerRecorder.h
  dtkCorePluginWidgetManager
  dtkCorePluginWidgetManager.h
  dtkCoreSettings
  dtkCoreSettings.h
  dtkCoreTypeTraits
  dtkCoreTypeTraits.h
  dtkCoreUtils
  dtkCoreUtils.h)

set(${PROJECT_NAME}_SOURCES
  dtkCoreApplication.cpp
  dtkCoreIOCompressor.cpp
  dtkCoreLayerManager.cpp
  dtkCoreMetaType.cpp
  dtkCoreObjectManager.cpp
  dtkCoreMetaContainerSequential.cpp
  dtkCoreParameter.cpp
  dtkCoreParameterInList.cpp
  dtkCoreParameterInListStringList.cpp
  dtkCoreParameterNumeric.cpp
  dtkCoreParameterPath.cpp
  dtkCoreParameterRange.cpp
  dtkCoreParameterSimple.cpp
  dtkCoreParameterObject.cpp
  dtkCoreParameterInListObject.cpp
  dtkCoreParameterInListStringListObject.cpp
  dtkCoreParameterPathObject.cpp
  dtkCoreParameterNumericObject.cpp
  dtkCoreParameterRangeObject.cpp
  dtkCoreParameterSimpleObject.cpp
  dtkCoreParameterCollection.cpp
  dtkCorePlugin.cpp
  dtkCorePluginFactory_p.cpp
  dtkCorePluginFactoryRecorder.cpp
  dtkCorePluginManagerHandler.cpp
  dtkCorePluginManagerRecorder.cpp
  dtkCorePluginWidgetManager.cpp)

## ###################################################################
## Config file
## ###################################################################

configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}Config.h.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.h")
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}Config.h.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.h"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config")

## #################################################################
## Build rules
## #################################################################

qt_add_library(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

target_include_directories(${PROJECT_NAME} PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/..
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>/..
  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
  $<INSTALL_INTERFACE:include>)

target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Core)
target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::Qml)

target_link_libraries(${PROJECT_NAME} PRIVATE ZLIB_INTERFACE)

target_link_libraries(${PROJECT_NAME} PUBLIC dtk::Log)

add_library(dtk::Core ALIAS dtkCore)

## #################################################################
## Target properties
## #################################################################

set_target_properties(${PROJECT_NAME} PROPERTIES VERSION   ${${PROJECT_NAME}_VERSION}
                                                 SOVERSION ${${PROJECT_NAME}_VERSION_MAJOR})

## #################################################################
## Export header file
## #################################################################

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export")
generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export"
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

## ###################################################################
## Install rules - files
## ###################################################################

install(
FILES
  ${${PROJECT_NAME}_HEADERS}
DESTINATION
  include/${PROJECT_NAME})

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

install(EXPORT ${PROJECT_NAME}-targets
FILE
  ${PROJECT_NAME}Targets.cmake
DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/dtkCore)

export(EXPORT ${PROJECT_NAME}-targets
FILE
  ${CMAKE_BINARY_DIR}/${PROJECT_NAME}Targets.cmake)

######################################################################
### CMakeLists.txt ends here
