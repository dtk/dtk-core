// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkCoreTypeTraits.h"
#include "dtkCoreUtils.h"

#include <QtCore>

#include <type_traits>

#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// dtkCoreMetaType functions implementations
// ///////////////////////////////////////////////////////////////////

namespace dtk {

    // Test conversion between list of types given as QMetaTypeId integers and template type
    namespace detail
    {
        template <typename T>
        inline bool can_convert(const QList<int>& types)
        {
            auto from = QMetaType::fromType<T>();
            for (int i_to : types) {
                QMetaType to(i_to);
                if (!QMetaType::canConvert(from, to)) {
                    return false;
                }
            }
            return true;
        }
    }

    template <typename T>
    inline bool canConvert(const QList<int>& types)
    {
        return ::dtk::detail::can_convert<T>(types);
    }


    // Creating of a variant by downcasting the class as much as possible
    namespace detail
    {
        // Default fallback to create QVariant
        template <typename T, typename E = void, typename F = void>
        struct variant_handler
        {
            static QVariant fromValue(const T& t) {
                return QVariant::fromValue(t);
            }
        };

        template <typename T>
        struct variant_handler<T, std::enable_if_t<::dtk::is_qobject<T>::value>, std::enable_if_t<!std::is_pointer<T>::value>>
        {
            static QVariant fromValue(const T& t) {
                auto mo = t.metaObject();
                auto mt = QMetaType::fromName(mo->className());
                // Try to find the most specific derived class to embed the object in the QVariant
                while (!mt.isValid()) {
                    mo = mo->superClass();
                    if (!mo) {
                        break;
                    }
                    mt = QMetaType::fromName(mo->className());
                }
                if (!mt.isValid()) {
                    return QVariant::fromValue(t);
                }
                return QVariant(mt, &t);
            }
        };

        template <typename T>
        struct variant_handler<T, std::enable_if_t<::dtk::is_qobject<T>::value>, std::enable_if_t<std::is_pointer<T>::value>>
        {
            static QVariant fromValue(const T& t) {
                auto mo = t->metaObject();
                QString class_name(mo->className());
                auto mt = QMetaType::fromName(qPrintable(class_name + "*"));
                // Try to find the most specific derived class to embed the object in the QVariant
                while (!mt.isValid()) {
                    mo = mo->superClass();
                    if (!mo) {
                        break;
                    }
                    class_name = mo->className();
                    mt = QMetaType::fromName(qPrintable(class_name + "*"));
                }
                if (!mt.isValid()) {
                    return QVariant::fromValue(t);
                }
                return QVariant(mt, &t);
            }
        };

        template <typename T>
        QVariant variant_from_value(const T& t)
        {
            return variant_handler<T>::fromValue(t);
        }
    }

    template <typename T>
    inline QVariant variantFromValue(const T& t)
    {
        return ::dtk::detail::variant_from_value(t);
    }

    template <typename... Args>
    inline QVariantList variantListFromTuple(const std::tuple<Args...>& t)
    {
        using TupleType = std::tuple<Args...>;
        constexpr std::size_t N = std::tuple_size_v<TupleType>;
        QVariantList list(N);
        std::size_t id = 0;
        ::dtk::for_each(t, [&](auto&& elt) {
            list[id++] = ::dtk::variantFromValue(elt);
        });
        return list;
    }

    template <typename... Args>
    inline QList<std::pair<QMetaType, std::uintptr_t>> metaTypeListFromTuple(const std::tuple<Args...>& t)
    {
        using TupleType = std::tuple<Args...>;
        constexpr std::size_t N = std::tuple_size_v<TupleType>;
        QList<std::pair<QMetaType, std::uintptr_t>> list(N);
        std::size_t id = 0;
        ::dtk::for_each(t, [&](auto&& elt) {
            using Type = std::decay_t<decltype(elt)>;
            auto address = reinterpret_cast<std::uintptr_t>(&elt);
            list[id++] = std::make_pair(QMetaType::fromType<Type>(), address);
        });
        return list;
    }

    // Cloning an object by trying to downcasting as much as possible so that it avoids slicing
    namespace detail {

        // Default clone fallback that returns nullptr
        template <typename T, typename E = void, typename F = void>
        struct clone_handler
        {
            static T *clone(const T *) {
                return nullptr;
            }
        };

        template <typename T>
        struct clone_handler<T, std::enable_if_t<::dtk::is_clonable<T>::value>>
        {
            static T *clone(const T *t) {
                using Type = std::remove_pointer_t<std::decay_t<T>>;
                return new Type(*t);
            }
        };

        template <typename T>
        std::enable_if_t<!::dtk::is_qobject<T>::value, T *> clone(const T *t)
        {
            return clone_handler<T>::clone(t);
        }

        template <typename T>
        std::enable_if_t<::dtk::is_qobject<T>::value, T *> clone(const T *t)
        {
            QString class_name(t->metaObject()->className());
            auto mt = QMetaType::fromName(qPrintable(class_name));
            if (!mt.isValid()) {
                return clone_handler<T>::clone(t);
            }
            return static_cast<T *>(mt.create(t));
        }
    }

    template <typename T>
    inline T *clone(const T *t)
    {
        return ::dtk::detail::clone(t);
    }


    // Copying an object by trying to downcasting as much as possible so that it avoids slicing
    namespace detail
    {

        // Default fallback returning nullptr
        template <typename T, typename E = void, typename F = void>
        struct copy_handler
        {
            static bool copy(const T *, T *) {
                return false;
            }
        };

        template <typename T>
        struct copy_handler<T, std::enable_if_t<::dtk::is_copyable<T>::value>>
        {
            static bool copy(const T *s, T *t) {
                *t = *s;
                return true;
            }
        };

        template <typename T>
        std::enable_if_t<!::dtk::is_qobject<T>::value, bool> copy(const T *s, T *t)
        {
            return copy_handler<T>::copy(s, t);
        }

        template <typename T>
        std::enable_if_t<::dtk::is_qobject<T>::value, bool> copy(const T *s, T *t)
        {
            QString t_class_name(t->metaObject()->className());
            auto t_mt = QMetaType::fromName(qPrintable(t_class_name));

            QString s_class_name(s->metaObject()->className());
            auto s_mt = QMetaType::fromName(qPrintable(s_class_name));

            if (!t_mt.isValid() || (t_mt != s_mt)) {
                return copy_handler<T>::copy(s, t);
            }
            t_mt.construct(t, s);
            return true;
        }
    }

    template <typename T>
    inline bool copy(const T *s, T *t)
    {
        return ::dtk::detail::copy(s, t);
    }


    // Assign operation (aka =) when type supports it
    namespace detail
    {
        template <typename T>
        ::dtk::enable_assignment<T> assign(T& lhs, const T& rhs)
        {
            lhs = rhs;
        }

        template <typename T>
        ::dtk::disable_assignment<T> assign(T&, const T&)
        {
            dtkTrace() << Q_FUNC_INFO << "Current type does not support assignment";
        }
    }

    template <typename T>
    inline void assign(T& lhs, const T& rhs)
    {
        ::dtk::detail::assign(lhs, rhs);
    }


    // AddAssign operation (aka +=) when type supports it
    namespace detail
    {
        template <typename T>
        ::dtk::enable_add_assignment<T> add_assign(T& lhs, const T& rhs)
        {
            lhs += rhs;
        }

        template <typename T>
        ::dtk::disable_add_assignment<T> add_assign(T&, const T&)
        {
            dtkTrace() << Q_FUNC_INFO << "Current type does not support += operation";
        }
    }

    template <typename T>
    inline void addAssign(T& lhs, const T& rhs)
    {
        ::dtk::detail::add_assign(lhs, rhs);
    }


    // SubAssign operation (aka +=) when type supports it
    namespace detail
    {
        template <typename T>
        ::dtk::enable_sub_assignment<T> sub_assign(T& lhs, const T& rhs)
        {
            lhs -= rhs;
        }

        template <typename T>
        ::dtk::disable_sub_assignment<T> sub_assign(T&, const T&)
        {
            dtkTrace() << Q_FUNC_INFO << "Current type does not support -= operation";
        }
    }

    template <typename T>
    inline void subAssign(T& lhs, const T& rhs)
    {
        ::dtk::detail::sub_assign(lhs, rhs);
    }


    // MulAssign operation (aka +=) when type supports it
    namespace detail
    {
        template <typename T>
        ::dtk::enable_mul_assignment<T> mul_assign(T& lhs, const T& rhs)
        {
            lhs *= rhs;
        }

        template <typename T>
        ::dtk::disable_mul_assignment<T> mul_assign(T&, const T&)
        {
            dtkTrace() << Q_FUNC_INFO << "Current type does not support *= operation.";
        }
    }

    template <typename T>
    inline void mulAssign(T& lhs, const T& rhs)
    {
        ::dtk::detail::mul_assign(lhs, rhs);
    }


    // DivAssign operation (aka +=) when type supports it
    namespace detail
    {
        template <typename T>
        ::dtk::enable_div_assignment<T> div_assign(T& lhs, const T& rhs)
        {
            lhs /= rhs;
        }

        template <typename T>
        ::dtk::disable_div_assignment<T> div_assign(T&, const T&)
        {
            dtkTrace() << Q_FUNC_INFO << "Current type does not support /= operation.";
        }
    }

    template <typename T>
    inline void divAssign(T& lhs, const T& rhs)
    {
        ::dtk::detail::div_assign(lhs, rhs);
    }
}


// /////////////////////////////////////////////////////////////////
// Stream operators implementation
// /////////////////////////////////////////////////////////////////


template <typename T>
inline QDataStream& operator << (QDataStream& s, const std::list<T>& l)
{
    s << quint64(l.size());
    for (auto it = l.begin(); it != l.end(); ++it) {
        s << ::dtk::variantFromValue(*it);
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, std::list<T>& l)
{
    quint64 c; s >> c;
    l.clear();

    for (quint64 i = 0; i < c; ++i) {
        QVariant var;
        s >> var;
        l.push_back(var.value<T>());
        if (s.atEnd()) {
            break;
        }
    }
    return s;
}

template <typename T>
inline QDataStream& operator << (QDataStream& s, const std::vector<T>& v)
{
    s << quint64(v.size());

    for (auto it = v.begin(); it != v.end(); ++it) {
        s << ::dtk::variantFromValue(*it);
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, std::vector<T>& v)
{
    v.clear();
    quint64 c; s >> c;
    v.resize(c);

    for (quint64 i = 0; i < c; ++i) {
        QVariant var; // Very important to instantiate a void QVariant at each step, otherwise, it keeps the same pointer T* to store the stream.
        s >> var;
        v[i] = var.value<T>();
    }
    return s;
}

template <typename T>
inline QDataStream& operator << (QDataStream& s, QList<T> *l)
{
    if (l) {
        s << qlonglong(l->size());
        for (auto it = l->begin(); it != l->end(); ++it) {
            s << ::dtk::variantFromValue(*it);
        }
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, QList<T> *& l)
{
    qlonglong c; s >> c;
    if (!l) {
        l = new QList<T>(c);
    } else {
        l->clear();
        l->resize(c);
    }

    for (qlonglong i = 0; i < c; ++i) {
        QVariant var;
        s >> var;
        (*l)[i] = var.value<T>();
    }

    return s;
}

template <typename T>
inline QDataStream& operator << (QDataStream& s, std::list<T> *l)
{
    if (l) {
        s << quint64(l->size());
        for (auto it = l->begin(); it != l->end(); ++it) {
            s << ::dtk::variantFromValue(*it);
        }
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, std::list<T> *& l)
{
    quint64 c; s >> c;
    if (!l) {
        l = new std::list<T>();
    } else {
        l->clear();
    }

    for (quint64 i = 0; i < c; ++i) {
        QVariant var;
        s >> var;
        l->push_back(var.value<T>());

        if (s.atEnd()) {
            break;
        }
    }
    return s;
}

template <typename T>
inline QDataStream& operator << (QDataStream& s, std::vector<T> *v)
{
    if (v) {
        s << quint64(v->size());
        for (auto it = v->begin(); it != v->end(); ++it) {
            s << ::dtk::variantFromValue(*it);
        }
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, std::vector<T> *& v)
{
    quint64 c; s >> c;
    if (!v) {
        v = new std::vector<T>(c);
    } else {
        v->clear();
        v->resize(c);
    }

    for (quint64 i = 0; i < c; ++i) {
        QVariant var; // Very important to instantiate a void QVariant at each step, otherwise, it keeps the same pointer T* to store the stream.
        s >> var;
        (*v)[i] = var.value<T>();
    }
    return s;
}

template <typename T>
inline QDataStream& operator << (QDataStream& s, const QList<T *>& v)
{
    s << qlonglong(v.size());

    for (auto it = v.begin(); it != v.end(); ++it) {
        s << ::dtk::variantFromValue(*it);
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, QList<T *>& v)
{
    v.clear();
    qlonglong c; s >> c;
    v.resize(c);

    for (qlonglong i = 0; i < c; ++i) {
        QVariant var;
        s >> var;
        v[i] = var.value<T *>();
    }
    return s;
}

template <typename T>
inline QDataStream& operator << (QDataStream& s, const std::list<T *>& l)
{
    s << quint64(l.size());

    for (auto it = l.begin(); it != l.end(); ++it) {
        s << ::dtk::variantFromValue(*it);
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, std::list<T *>& l)
{
    l.clear();
    quint64 c; s >> c;

    for (quint64 i = 0; i < c; ++i) {
        QVariant var;
        s >> var;
        l.push_back(var.value<T *>());

        if (s.atEnd()) {
            break;
        }
    }
    return s;
}

template <typename T>
inline QDataStream& operator << (QDataStream& s, const std::vector<T *>& v)
{
    s << quint64(v.size());

    for (auto it = v.begin(); it != v.end(); ++it) {
        s << ::dtk::variantFromValue(*it);
    }
    return s;
}

template <typename T>
inline QDataStream& operator >> (QDataStream& s, std::vector<T *>& v)
{
    v.clear();
    quint64 c; s >> c;
    v.resize(c);

    for (quint64 i = 0; i < c; ++i) {
        QVariant var; // Very important to instantiate a void QVariant at each step, otherwise, it keeps the same pointer T* to store the stream.
        s >> var;
        v[i] = var.value<T *>();
    }
    return s;
}

//
// dtkCoreMetaType.tpp ends here
