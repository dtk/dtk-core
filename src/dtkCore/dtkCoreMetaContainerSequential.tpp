// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once


// /////////////////////////////////////////////////////////////////
// dtkCoreMetaContainerSequential::item implementation
// /////////////////////////////////////////////////////////////////

inline dtkCoreMetaContainerSequential::item::item(handler::iterator_ptr iterator) : it(std::move(iterator))
{
}

inline dtkCoreMetaContainerSequential::item::item(const item& o) : it(o.it->clone())
{
}

inline dtkCoreMetaContainerSequential::item::item(item&& o)
{
    std::swap(it, o.it);
}

inline dtkCoreMetaContainerSequential::item::~item(void)
{
    it.reset();
}

inline auto dtkCoreMetaContainerSequential::item::operator = (const item& o) -> item&
{
    if (this != &o) {
        it->copy(*(o.it));
    }
    return *this;
}

inline auto dtkCoreMetaContainerSequential::item::operator = (item&& o) -> item&
{
    std::swap(it, o.it);
    return *this;
}

inline bool dtkCoreMetaContainerSequential::item::operator == (const item& o) const
{
    return  it->equal(*(o.it));
}

inline bool dtkCoreMetaContainerSequential::item::operator != (const item& o) const
{
    return !it->equal(*(o.it));
}

inline const QVariant dtkCoreMetaContainerSequential::item::value(void) const
{
    return it->variant();
}

template <typename T>
inline const T& dtkCoreMetaContainerSequential::item::value(void) const
{
    return *static_cast<const T *>(it->value());
}

template <typename T>
inline auto dtkCoreMetaContainerSequential::item::operator = (const T& t) -> item&
{
    it->assign(&t);
    return *this;
}

template <typename T>
inline auto dtkCoreMetaContainerSequential::item::operator += (const T& t) -> item&
{
    it->addAssign(&t);
    return *this;
}

template <typename T>
inline auto dtkCoreMetaContainerSequential::item::operator -= (const T& t) -> item&
{
    it->subAssign(&t);
    return *this;
}

template <typename T>
inline auto dtkCoreMetaContainerSequential::item::operator *= (const T& t) -> item&
{
    it->mulAssign(&t);
    return *this;
}

template <typename T>
inline auto dtkCoreMetaContainerSequential::item::operator /= (const T& t) -> item&
{
    it->divAssign(&t);
    return *this;
}

template<typename T>
inline bool dtkCoreMetaContainerSequential::item::operator == (const T& t) const
{
    return it->equalToValue(&t);
}

template<typename T>
inline bool dtkCoreMetaContainerSequential::item::operator != (const T& t) const
{
    return !it->equalToValue(&t);
}

// /////////////////////////////////////////////////////////////////

inline QDebug& operator << (QDebug debug, const dtkCoreMetaContainerSequential::item& item)
{
    const bool oldSetting = debug.autoInsertSpaces();
    debug.nospace() << item.value();
    debug.setAutoInsertSpaces(oldSetting);
    return debug.maybeSpace();
}

// /////////////////////////////////////////////////////////////////
// dtkCoreMetaContainerSequential::iterator implementation
// /////////////////////////////////////////////////////////////////

inline dtkCoreMetaContainerSequential::iterator::iterator(handler::iterator_ptr iterator) : proxy(std::move(iterator))
{
}

inline dtkCoreMetaContainerSequential::iterator::iterator(const iterator& o) : proxy(o.proxy)
{
}

inline dtkCoreMetaContainerSequential::iterator::iterator(iterator&& o) : proxy(std::move(o.proxy))
{

}

inline dtkCoreMetaContainerSequential::iterator::~iterator(void)
{
}

inline auto dtkCoreMetaContainerSequential::iterator::operator = (const iterator& o) -> iterator&
{
    proxy = o.proxy;
    return *this;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator = (iterator&& o) -> iterator&
{
    std::swap(proxy.it, o.proxy.it);
    return *this;
}

inline bool dtkCoreMetaContainerSequential::iterator::operator == (const iterator& o) const
{
    return proxy == o.proxy;
}

inline bool dtkCoreMetaContainerSequential::iterator::operator != (const iterator& o) const
{
    return proxy != o.proxy;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator * (void) -> item&
{
    return proxy;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator [] (long long j) ->item&
{
    return *(*this + j);
}

template <typename T>
inline T& dtkCoreMetaContainerSequential::iterator::value(void)
{
    return *reinterpret_cast<T *>(proxy.it->value());
}

inline auto dtkCoreMetaContainerSequential::iterator::operator ++ (void) -> iterator&
{
    proxy.it->advance();
    return *this;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator ++ (int) -> iterator
{
    iterator o(*this);
    proxy.it->advance();
    return o;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator -- (void) -> iterator&
{
    proxy.it->moveBackward(static_cast<long long>(1));
    return *this;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator -- (int) -> iterator
{
    iterator o(*this);
    proxy.it->moveBackward(static_cast<long long>(1));
    return o;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator += (long long j) -> iterator&
{
    proxy.it->moveForward(j);
    return *this;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator -= (long long j) -> iterator&
{
    proxy.it->moveBackward(j);
    return *this;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator + (long long j) const -> iterator
{
    iterator o(*this);
    o += j;
    return o;
}

inline auto dtkCoreMetaContainerSequential::iterator::operator - (long long j) const -> iterator
{
    iterator o(*this);
    o -= j;
    return o;
}

// /////////////////////////////////////////////////////////////////
// dtkCoreMetaContainerSequential::const_iterator implementation
// /////////////////////////////////////////////////////////////////

inline dtkCoreMetaContainerSequential::const_iterator::const_iterator(handler::const_iterator_ptr iterator) : it(std::move(iterator))
{
}

inline dtkCoreMetaContainerSequential::const_iterator::const_iterator(const const_iterator& o) : it(o.it->clone())
{
}

inline dtkCoreMetaContainerSequential::const_iterator::const_iterator(const_iterator&& o)
{
    std::swap(it, o.it);
}

inline dtkCoreMetaContainerSequential::const_iterator::~const_iterator(void)
{
    it.reset();
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator = (const const_iterator& o) -> const_iterator&
{
    if (this != &o) {
        it->copy(*(o.it));
    }
    return *this;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator = (const_iterator&& o) -> const_iterator&
{
    std::swap(it, o.it);
    return *this;
}

inline bool dtkCoreMetaContainerSequential::const_iterator::operator == (const const_iterator& o) const
{
    return it->equal(*(o.it));
}

inline bool dtkCoreMetaContainerSequential::const_iterator::operator != (const const_iterator& o) const
{
    return !it->equal(*(o.it));
}

inline QVariant dtkCoreMetaContainerSequential::const_iterator::operator * (void) const
{
    return it->variant();
}

inline QVariant dtkCoreMetaContainerSequential::const_iterator::operator [] (long long j) const
{
    return *(*this + j);
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator ++ (void) -> const_iterator&
{
    it->advance();
    return *this;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator ++ (int) -> const_iterator
{
    const_iterator o(*this);
    it->advance();
    return o;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator -- (void) -> const_iterator&
{
    it->moveBackward(static_cast<long long>(1));
    return *this;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator -- (int) -> const_iterator
{
    const_iterator o(*this);
    it->moveBackward(static_cast<long long>(1));
    return o;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator += (long long j) -> const_iterator&
{
    it->moveForward(j);
    return *this;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator -= (long long j) -> const_iterator&
{
    it->moveBackward(j);
    return *this;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator + (long long j) const -> const_iterator
{
    const_iterator o(*this);
    o += j;
    return o;
}

inline auto dtkCoreMetaContainerSequential::const_iterator::operator - (long long j) const -> const_iterator
{
    const_iterator o(*this);
    o -= j;
    return o;
}

// /////////////////////////////////////////////////////////////////
// dtkCoreMetaContainerSequential implementation
// /////////////////////////////////////////////////////////////////

inline dtkCoreMetaContainerSequential::dtkCoreMetaContainerSequential(handler *h) : m_h(handler_ptr(h))
{
    if (m_h) {
        m_proxy.it = m_h->begin();
    }
}

inline dtkCoreMetaContainerSequential::dtkCoreMetaContainerSequential(dtkCoreMetaContainerSequential&& o) : m_h(std::move(o.m_h))
{
    if (m_h) {
        m_proxy.it = m_h->begin();
    }
}

inline dtkCoreMetaContainerSequential::dtkCoreMetaContainerSequential(const dtkCoreMetaContainerSequential& o)
{
    if (o.m_h) {
        m_h = handler_ptr(o.m_h->clone());
        m_proxy.it = m_h->begin();
    }
}

inline dtkCoreMetaContainerSequential::~dtkCoreMetaContainerSequential(void)
{
    m_h.reset();
}

inline dtkCoreMetaContainerSequential& dtkCoreMetaContainerSequential::operator = (const dtkCoreMetaContainerSequential& o)
{
    if (this != &o) {
        if (!o.m_h) {
            m_h.reset();
            m_proxy.it.reset();
        } else {
            m_h = handler_ptr(o.m_h->clone());
            m_proxy.it = m_h->begin();
        }
    }
    return *this;
}

inline dtkCoreMetaContainerSequential& dtkCoreMetaContainerSequential::operator = (dtkCoreMetaContainerSequential&& o)
{
    std::swap(m_h, o.m_h);
    m_proxy = std::move(o.m_proxy);
    return *this;
}

inline bool dtkCoreMetaContainerSequential::hasBiDirectionalIterator(void) const
{
    return m_h->hasBiDirectionalIterator();
}

inline bool dtkCoreMetaContainerSequential::hasRandomAccessIterator(void) const
{
    return m_h->hasRandomAccessIterator();
}

inline bool dtkCoreMetaContainerSequential::empty(void) const
{
    return m_h->empty();
}

inline long long dtkCoreMetaContainerSequential::size(void) const
{
    return m_h->size();
}

inline void dtkCoreMetaContainerSequential::clear(void)
{
    m_h->clear();
}

inline void dtkCoreMetaContainerSequential::reserve(long long size)
{
    m_h->reserve(size);
}

inline void dtkCoreMetaContainerSequential::resize(long long size)
{
    m_h->resize(size);
}

template <typename T> inline void dtkCoreMetaContainerSequential::append(const T& t)
{
    m_h->append(&t);
}

template <typename T> inline void dtkCoreMetaContainerSequential::prepend(const T& t)
{
    m_h->prepend(&t);
}

inline void dtkCoreMetaContainerSequential::append(const QVariant& v)
{
    this->insert(m_h->size(), v);
}

inline void dtkCoreMetaContainerSequential::prepend(const QVariant& v)
{
    this->insert(0, v);
}

template <typename T>
inline void dtkCoreMetaContainerSequential::insert(long long idx, const T& t)
{
    m_h->insert(idx, &t);
}

template <typename T>
inline void dtkCoreMetaContainerSequential::setAt(long long idx, const T& t)
{
    m_h->setAt(idx, &t);
}

template <typename T>
inline const T& dtkCoreMetaContainerSequential::at(long long idx) const
{
    return *static_cast<const T *>(m_h->at(idx));
}

inline const QVariant& dtkCoreMetaContainerSequential::at(long long idx) const
{
    m_h->variantAt(idx, m_var);
    return m_var;
}

inline void dtkCoreMetaContainerSequential::removeAt(long long idx)
{
    m_h->removeAt(idx);
}

inline const QVariant& dtkCoreMetaContainerSequential::first(void) const
{
    m_h->variantAt(0, m_var);
    return m_var;
}

inline auto dtkCoreMetaContainerSequential::first(void) -> item&
{
    m_proxy.it = m_h->begin();
    return m_proxy;
}

inline const QVariant& dtkCoreMetaContainerSequential::last(void) const
{
    m_h->variantAt(m_h->size() - 1, m_var);
    return m_var;
}

inline auto dtkCoreMetaContainerSequential::last(void) -> item&
{
    m_proxy.it = m_h->begin();
    m_proxy.it->moveForward(m_h->size() - 1);
    return m_proxy;
}

inline const QVariant& dtkCoreMetaContainerSequential::operator [] (long long idx) const
{
    m_h->variantAt(idx, m_var);
    return m_var;
}

inline auto dtkCoreMetaContainerSequential::operator [] (long long idx) -> item&
{
    m_h->iteratorAt(idx, m_proxy.it);
    return m_proxy;
}

inline auto dtkCoreMetaContainerSequential::begin(void) -> iterator
{
    return iterator(m_h->begin());
}

inline auto dtkCoreMetaContainerSequential::end(void) -> iterator
{
    return iterator(m_h->end());
}

inline auto dtkCoreMetaContainerSequential::begin(void) const -> const_iterator
{
    return const_iterator(m_h->cbegin());
}

inline auto dtkCoreMetaContainerSequential::end(void) const -> const_iterator
{
    return const_iterator(m_h->cend());
}

inline auto dtkCoreMetaContainerSequential::cbegin(void) const -> const_iterator
{
    return const_iterator(m_h->cbegin());
}

inline auto dtkCoreMetaContainerSequential::cend(void) const -> const_iterator
{
    return const_iterator(m_h->cend());
}

// ///////////////////////////////////////////////////////////////////
// Debug operator
// ///////////////////////////////////////////////////////////////////

inline QDebug operator << (QDebug debug, const dtkCoreMetaContainerSequential& container)
{
    debug << qPrintable(container.m_h->description());
    return debug.maybeSpace();
}

// ///////////////////////////////////////////////////////////////////
// Register converter to dtkCoreMetaContainerSequential
// ///////////////////////////////////////////////////////////////////

namespace dtk
{
    // Functor that converts container pointer to dtkCoreMetaContainerSequentialHandler
    namespace detail
    {
        template <typename T>
        struct converter_to_meta_container_sequential_impl
        {
        };

        template <typename T>
        struct converter_to_meta_container_sequential_impl<T *>
        {
            dtkCoreMetaContainerSequential operator () (T *container) const
            {
                return dtkCoreMetaContainerSequential(new dtkCoreMetaContainerSequentialHandlerTemplate<T>(container));
            }
        };
    }

    template <typename T>
    using converterToMetaContainerSequential = detail::converter_to_meta_container_sequential_impl<T>;


    // Registration of the above converter to the QMetaType system
    namespace detail
    {
        template <typename T>
        std::enable_if_t<!::dtk::is_container_value_type_meta_type<T>::value, bool>
        register_converter_to_meta_container_sequential_impl(int)
        {
            return false;
        }

        template <typename T>
        std::enable_if_t<::dtk::is_container_value_type_meta_type<T>::value, bool>
        register_converter_to_meta_container_sequential_impl(int id)
        {
            using meta_seq_c = dtkCoreMetaContainerSequential;
            using converter = ::dtk::converterToMetaContainerSequential<T>;

            auto mt_from = QMetaType(id);
            auto mt_to = QMetaType::fromType<meta_seq_c>();

            if (!QMetaType::hasRegisteredConverterFunction(mt_from, mt_to)) {
                converter o;
                return QMetaType::registerConverter<T, meta_seq_c, converter>(o);
            }
            return true;
        }

        template <typename T>
        std::enable_if_t<!::dtk::is_sequential_container_pointer<T>::value, bool>
        register_converter_to_meta_container_sequential(int)
        {
            return false;
        }

        template <typename T>
        std::enable_if_t<::dtk::is_sequential_container_pointer<T>::value, bool>
        register_converter_to_meta_container_sequential(int id)
        {
            return register_converter_to_meta_container_sequential_impl<T>(id);
        }
    }

    template <typename T>
    inline bool registerConverterToMetaContainerSequential(int id)
    {
        return detail::register_converter_to_meta_container_sequential<T>(id);
    }
}

//
// dtkCoreMetaContainerSequential.tpp ends here
