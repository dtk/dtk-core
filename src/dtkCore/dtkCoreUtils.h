// dtkCoreUtils.h ---
//

#pragma once

#include <type_traits>

// /////////////////////////////////////////////////////////////////////////////

namespace dtk {

    // /////////////////////////////////////////////////////////////////////////
    // Iterating at runtime over a tuple in order to apply a functor
    namespace detail
    {
        template <class Tuple, class F, std::size_t... I>
        constexpr F for_each(Tuple&& t, F&& f, std::index_sequence<I...>)
        {
            (f(std::get<I>(std::forward<Tuple>(t))),...);
            return f;
        }
    }

    template <class Tuple, class F>
    constexpr F for_each(Tuple&& t, F&& f)
    {
        constexpr std::size_t N = std::tuple_size_v<std::remove_reference_t<Tuple>>;
        return detail::for_each(std::forward<Tuple>(t), std::forward<F>(f), std::make_index_sequence<N>{});
    }
}

//
// dtkCoreUtils.h ends here
