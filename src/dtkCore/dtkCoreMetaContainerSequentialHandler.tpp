// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

// ///////////////////////////////////////////////////////////////////
// dtkCoreMetaContainerSequentialHandlerTemplate::iterator
// ///////////////////////////////////////////////////////////////////

template < typename T >
inline dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::iterator(const container_iterator& iterator) : it(iterator)
{

}

template < typename T >
inline auto dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::clone(void) const -> handler_iterator_ptr
{
    return handler_iterator_ptr(new iterator(it));
}

template < typename T >
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::copy(const handler_iterator& o)
{
    it = static_cast<const iterator&>(o).it;
}

template < typename T >
inline bool dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::equal(const handler_iterator& o) const
{
    return it == static_cast<const iterator&>(o).it;
}

template < typename T >
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::advance(void)
{
    ++it;
}

template < typename T >
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::moveForward(long long step)
{
    std::advance(it, step);
}

template < typename T >
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::moveBackward(long long step)
{
    std::advance(it, -step);
}

template < typename T >
inline void *dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::value(void) const
{
    return &(*it);
}

template < typename T >
inline QVariant dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::variant(void) const
{
    return QVariant(QMetaType::fromType<container_value_type>(), &(*it));
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::assign(const void *t)
{
    dtk::assign(*it, *static_cast<const container_value_type *>(t));
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::addAssign(const void *t)
{
    dtk::addAssign(*it, *static_cast<const container_value_type *>(t));
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::subAssign(const void *t)
{
    dtk::subAssign(*it, *static_cast<const container_value_type *>(t));
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::mulAssign(const void *t)
{
    dtk::mulAssign(*it, *static_cast<const container_value_type *>(t));
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::divAssign(const void *t)
{
    dtk::divAssign(*it, *static_cast<const container_value_type *>(t));
}

template <typename T>
inline bool dtkCoreMetaContainerSequentialHandlerTemplate<T>::iterator::equalToValue(const void *t) const
{
    return (*it == *static_cast<const container_value_type *>(t));
}

// ///////////////////////////////////////////////////////////////////
// dtkCoreMetaContainerSequentialHandlerTemplate::const_iterator
// ///////////////////////////////////////////////////////////////////

template <typename T>
inline dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::const_iterator(const container_const_iterator& iterator) : it(iterator)
{

}

template <typename T>
inline auto dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::clone(void) const -> handler_const_iterator_ptr
{
    return handler_const_iterator_ptr(new const_iterator(it));
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::copy(const handler_const_iterator& o)
{
    it = static_cast<const const_iterator&>(o).it;
}

template <typename T>
inline bool dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::equal(const handler_const_iterator& o) const
{
    return it == static_cast<const const_iterator&>(o).it;
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::advance(void)
{
    ++it;
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::moveForward(long long step)
{
    std::advance(it, step);
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::moveBackward(long long step)
{
    std::advance(it, -step);
}

template < typename T >
inline const void *dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::value(void) const
{
    return &(*it);
}

template <typename T>
inline QVariant dtkCoreMetaContainerSequentialHandlerTemplate<T>::const_iterator::variant(void) const
{
    return QVariant(QMetaType::fromType<container_value_type>(), &(*it));
}

// ///////////////////////////////////////////////////////////////////
// dtkCoreMetaContainerSequentialHandlerTemplate
// ///////////////////////////////////////////////////////////////////

template <typename T>
inline dtkCoreMetaContainerSequentialHandlerTemplate<T>::dtkCoreMetaContainerSequentialHandlerTemplate(T *c) : m_container(c)
{
}

template <typename T>
inline dtkCoreMetaContainerSequentialHandlerTemplate<T>::~dtkCoreMetaContainerSequentialHandlerTemplate(void)
{
    m_container = nullptr;
}

template <typename T>
inline auto dtkCoreMetaContainerSequentialHandlerTemplate<T>::clone(void) -> dtkCoreMetaContainerSequentialHandler*
{
    return new dtkCoreMetaContainerSequentialHandlerTemplate<T>(m_container);
}

template <typename T>
inline QString dtkCoreMetaContainerSequentialHandlerTemplate<T>::description(void) const
{
    QString desc;
    QDebug dbg(&desc);
    dbg.nospace() << QMetaType::fromType<T *>().name();
    dbg.nospace() << ", size = ";
    dbg.nospace() << this->size();
    dbg.nospace() << ", (";

    for (long long i = 0; i < this->size(); ++i) {
        if (i)
            dbg << ", ";

        dbg << *static_cast<const container_value_type *>(this->at(i));
    }

    dbg << ')';
    return desc;
}

template <typename T>
inline bool dtkCoreMetaContainerSequentialHandlerTemplate<T>::hasBiDirectionalIterator(void) const
{
    return dtk::is_bidirectional<typename T::iterator>::value;
}

template <typename T>
inline bool dtkCoreMetaContainerSequentialHandlerTemplate<T>::hasRandomAccessIterator(void) const
{
    return dtk::is_random_access<typename T::iterator>::value;
}

template <typename T>
inline bool dtkCoreMetaContainerSequentialHandlerTemplate<T>::empty(void) const
{
    return m_container->empty();
}

template <typename T>
inline long long dtkCoreMetaContainerSequentialHandlerTemplate<T>::size(void) const
{
    return m_container->size();
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::clear(void)
{
    m_container->clear();
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::reserve(long long size)
{
    this->reserve_impl<T>(size);
}

template <typename T> template <typename U>
inline dtk::enable_reserve<U> dtkCoreMetaContainerSequentialHandlerTemplate<T>::reserve_impl(long long size)
{
    m_container->reserve(size);
}

template <typename T> template <typename U>
inline dtk::disable_reserve<U> dtkCoreMetaContainerSequentialHandlerTemplate<T>::reserve_impl(long long)
{
    dtkTrace() << Q_FUNC_INFO << "This type of container does not support reserve operation.";
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::resize(long long size)
{
    this->resize_impl<T>(size);
}

template <typename T> template <typename U>
inline dtk::enable_resize<U> dtkCoreMetaContainerSequentialHandlerTemplate<T>::resize_impl(long long size)
{
    m_container->resize(size);
}

template <typename T> template <typename U>
inline dtk::disable_resize<U> dtkCoreMetaContainerSequentialHandlerTemplate<T>::resize_impl(long long)
{
    dtkTrace() << Q_FUNC_INFO << "This type of container does not support resize operation.";
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::append(const void *t)
{
    this->insert(this->size(), t);
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::prepend(const void *t)
{
    this->insert(0, t);
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::insert(long long idx, const void *t)
{
    typename T::iterator it(m_container->begin());
    std::advance(it, idx);
    m_container->insert(it, *static_cast<const container_value_type *>(t));
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::setAt(long long idx, const void *t)
{
    typename T::iterator it(m_container->begin());
    std::advance(it, idx);
    *it = *static_cast<const container_value_type *>(t);
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::removeAt(long long idx)
{
    typename T::iterator it(m_container->begin());
    std::advance(it, idx);
    m_container->erase(it);
}

template <typename T>
inline const void *dtkCoreMetaContainerSequentialHandlerTemplate<T>::at(long long idx) const
{
    typename T::const_iterator it(m_container->begin());
    std::advance(it, idx);
    return &(*it);
}

template <typename T>
inline void *dtkCoreMetaContainerSequentialHandlerTemplate<T>::at(long long idx)
{
    typename T::iterator it(m_container->begin());
    std::advance(it, idx);
    return &(*it);
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::variantAt(long long idx, QVariant& var)
{
    typename T::const_iterator it(m_container->cbegin());
    std::advance(it, idx);
    var.setValue(*it);
}

template <typename T>
inline void dtkCoreMetaContainerSequentialHandlerTemplate<T>::iteratorAt(long long idx, handler_iterator_ptr& pit)
{
    auto hit = static_cast<iterator *>(pit.get());
    hit->it = m_container->begin();
    std::advance(hit->it, idx);
}

template <typename T>
inline auto dtkCoreMetaContainerSequentialHandlerTemplate<T>::begin(void) -> handler_iterator_ptr
{
    return handler_iterator_ptr(new iterator(m_container->begin()));
}

template <typename T>
inline auto dtkCoreMetaContainerSequentialHandlerTemplate<T>::cbegin(void) const -> handler_const_iterator_ptr
{
    return handler_const_iterator_ptr(new const_iterator(m_container->cbegin()));
}

template <typename T>
inline auto dtkCoreMetaContainerSequentialHandlerTemplate<T>::end(void) -> handler_iterator_ptr
{
    return handler_iterator_ptr(new iterator(m_container->end()));
}

template <typename T>
inline auto dtkCoreMetaContainerSequentialHandlerTemplate<T>::cend(void) const -> handler_const_iterator_ptr
{
    return handler_const_iterator_ptr(new const_iterator(m_container->cend()));
}

//
// dtkCoreMetaContainerSequentialHandler.tpp ends here
