#!/bin/bash

if [[ -d build ]]; then
    rm -rf build
fi
mkdir build
cd build

if [[ "$c_compiler" == "gcc" ]]; then
  export PATH="${PATH}:${BUILD_PREFIX}/${HOST}/sysroot/usr/lib:${BUILD_PREFIX}/${HOST}/sysroot/usr/include"
fi


ARCH=`arch`
if [ $ARCH = "arm64" ] || [ $ARCH = "aarch64" ]; then
  export QT_HOST_PATH=$PREFIX
fi

if [ `uname` = "Darwin" ]; then
    cmake .. \
          -G 'Ninja' \
          -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
          -DCMAKE_INSTALL_LIBDIR=lib \
          -DCMAKE_BUILD_TYPE=Release \
          -DZLIB_LIBRARY_RELEASE=$PREFIX/lib/libz.dylib
else
    cmake .. \
          -G 'Ninja' \
          -DCMAKE_INSTALL_PREFIX="${PREFIX}" \
          -DCMAKE_INSTALL_LIBDIR=lib \
          -DCMAKE_BUILD_TYPE=Release \
          -DZLIB_LIBRARY_RELEASE=$PREFIX/lib/libz.so
fi

cmake --build . --target install
